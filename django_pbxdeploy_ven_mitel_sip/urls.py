from django.urls import include, path, re_path
from django.contrib import admin
from django.http import HttpResponseRedirect, HttpResponse
from django.views.defaults import page_not_found

from django_pbxdeploy_ven_mitel_sip.django_pbxdeploy_ven_mitel_sip import views

urlpatterns = [
    # GLOBAL config
    # tuz files are used to encrypt the phones config files
    # this method is only supported on Mitel devices
    # the encryptor is only available for windows, thus not going to be supported
    # path('security.tuz', views.gen_config),
    path("aastra.cfg", views.gen_config_startup_cfg),
    path("startup.cfg", views.gen_config_startup_cfg),
    # lic files are not described in Mitel docs, thus ignored
    # path('startup.lic', views.gen_config_startup_lic),
    # MODEL config
    path("6863i.cfg", views.gen_config_model_6863i),
    path("6865i.cfg", views.gen_config_model_6865i),
    path("6867i.cfg", views.gen_config_model_6867i),
    path("6869i.cfg", views.gen_config_model_6869i),
    path("6873i.cfg", views.gen_config_model_6873i),
    path("6920.cfg", views.gen_config_model_6920),
    path("6930.cfg", views.gen_config_model_6930),
    path("6940.cfg", views.gen_config_model_6940),
    # ENDPOINT config
    re_path(r"^[a-fA-F0-9]{12}\.cfg$", views.gen_config_endpoint),
    # re_path(r'^[a-fA-F0-9]{12}\.lic$', views.gen_config_endpoint),
    # VENDOR ESSENTIAL FILES
    path("lang_<lang_code>.txt", views.get_vendor_media_essentials_lang),
    path("syslang_<lang_code>.txt", views.get_vendor_media_essentials_syslang),
    path("crt_<cert_name>.pem", views.get_vendor_media_essentials_cert),
]
