import os

from django.core.management.base import BaseCommand

from django_pbxdeploy_models.django_pbxdeploy_models.models import CusMedia, CusRealm


class Command(BaseCommand):
    vendor = "mitel"

    model = "sip"

    help = "Import essential files for " + vendor + " " + model

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        vendor_folder = os.path.join("data", self.vendor, self.model)

        for filename in os.listdir(vendor_folder):
            print(filename)

            media_name = "vendor_" + self.vendor + "_" + self.model + "_" + filename

            try:
                media_entry = CusMedia.objects.get(name=media_name)
            except CusMedia.DoesNotExist:
                media_entry = CusMedia()

            media_entry.name = media_name
            media_entry.realm = CusRealm.objects.get(id=1)
            media_entry.data = open(os.path.join(vendor_folder, filename), "rb").read()
            media_entry.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
