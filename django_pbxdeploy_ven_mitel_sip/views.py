from django.http import HttpResponse

from django_pbxdeploy_main.django_pbxdeploy_main.endpoint_digest_auth import (
    EndpointDigestAuth,
)
from django_pbxdeploy_main.django_pbxdeploy_main.models import ConfigMap
from django_pbxdeploy_models.django_pbxdeploy_models.models import *


def gen_config_startup_cfg(request):
    config = ""

    startup_config = CusDeployPhone.objects.filter(phone_model=3)

    for confvar in startup_config:
        if confvar.default_value is None:
            if confvar.phone_var.default_value is None:
                continue
            else:
                config = (
                    config
                    + confvar.special_name
                    + confvar.phone_model.conf_seperator
                    + confvar.phone_var.default_value
                    + "\r\n"
                )
        else:
            config = (
                config
                + confvar.special_name
                + confvar.phone_model.conf_seperator
                + confvar.default_value
                + "\r\n"
            )

    return HttpResponse(config)


def gen_config_model_6863i(request):
    deploy_phone = CusDeployPhone.objects.filter(phone_model=9)

    return gen_config_model(request, deploy_phone)


def gen_config_model_6865i(request):
    deploy_phone = CusDeployPhone.objects.filter(phone_model=10)

    return gen_config_model(request, deploy_phone)


def gen_config_model_6867i(request):
    deploy_phone = CusDeployPhone.objects.filter(phone_model=11)

    return gen_config_model(request, deploy_phone)


def gen_config_model_6869i(request):
    deploy_phone = CusDeployPhone.objects.filter(phone_model=2)

    return gen_config_model(request, deploy_phone)


def gen_config_model_6873i(request):
    deploy_phone = CusDeployPhone.objects.filter(phone_model=12)

    return gen_config_model(request, deploy_phone)


def gen_config_model_6920(request):
    deploy_phone = CusDeployPhone.objects.filter(phone_model=13)

    return gen_config_model(request, deploy_phone)


def gen_config_model_6930(request):
    deploy_phone = CusDeployPhone.objects.filter(phone_model=14)

    return gen_config_model(request, deploy_phone)


def gen_config_model_6940(request):
    deploy_phone = CusDeployPhone.objects.filter(phone_model=15)

    return gen_config_model(request, deploy_phone)


def gen_config_model(request, deploy_phone):
    deploy_phone_general = CusDeployPhone.objects.filter(phone_model=7)

    config_list = ConfigMap()

    for entry in deploy_phone:
        config_list.add(entry)

    for entry in deploy_phone_general:
        config_list.add(entry)

    config = ""

    for model_var in config_list.get():
        if model_var.phone_var.varid == 51:
            config = (
                config
                + model_var.special_name
                + model_var.phone_model.conf_seperator
                + deploy_phone.first().phone_model.firmware
                + "\r\n"
            )
            continue

        if model_var.default_value is not None:
            config = (
                config
                + model_var.special_name
                + model_var.phone_model.conf_seperator
                + model_var.default_value
                + "\r\n"
            )
        else:
            if model_var.phone_var.default_value is not None:
                config = (
                    config
                    + model_var.special_name
                    + model_var.phone_model.conf_seperator
                    + model_var.phone_var.default_value
                    + "\r\n"
                )

    return HttpResponse(config)


def gen_config_endpoint(request):
    digest = EndpointDigestAuth(realm="CloudPBX Deployment")
    auth = digest.is_authenticated(request=request)

    if auth is not True:
        return auth

    endpoint_auth = request.phone_auth

    config = ""

    endpoint_config = CusDeployConf.objects.filter(endpoint=endpoint_auth.username)
    endpoint_settings = CusPhoneSettings.objects.get(endpoint=endpoint_auth.username)
    deploy_phone_general = CusDeployPhone.objects.filter(phone_model=8)
    deploy_model = CusDeployPhone.objects.filter(
        phone_model=endpoint_settings.phone_model
    )

    config_list = ConfigMap()

    for entry in deploy_model:
        config_list.add(entry)

    for entry in deploy_phone_general:
        config_list.add(entry)

    for endpoint_var in config_list.get():
        if endpoint_var.phone_var.varid == 2:
            config = (
                config
                + endpoint_var.special_name
                + endpoint_var.phone_model.conf_seperator
                + endpoint_auth.username
                + "\r\n"
            )
        elif endpoint_var.phone_var.varid == 3:
            config = (
                config
                + endpoint_var.special_name
                + endpoint_var.phone_model.conf_seperator
                + endpoint_auth.username
                + "\r\n"
            )
        elif endpoint_var.phone_var.varid == 4:
            config = (
                config
                + endpoint_var.special_name
                + endpoint_var.phone_model.conf_seperator
                + endpoint_auth.password
                + "\r\n"
            )
        elif endpoint_var.phone_var.varid == 5:
            config = (
                config
                + endpoint_var.special_name
                + endpoint_var.phone_model.conf_seperator
                + endpoint_settings.display_name
                + "\r\n"
            )
        elif endpoint_var.phone_var.varid == 6:
            config = (
                config
                + endpoint_var.special_name
                + endpoint_var.phone_model.conf_seperator
                + str(endpoint_settings.telnum_int)
                + " - "
                + endpoint_settings.display_name
                + "\r\n"
            )
        elif endpoint_var.phone_var.varid == 65:
            config = (
                config
                + endpoint_var.special_name
                + endpoint_var.phone_model.conf_seperator
                + endpoint_auth.username
                + "\r\n"
            )
        elif endpoint_var.phone_var.varid == 66:
            config = (
                config
                + endpoint_var.special_name
                + endpoint_var.phone_model.conf_seperator
                + endpoint_auth.password
                + "\r\n"
            )
        else:
            for endpoint_cfg in endpoint_config:
                if endpoint_var.phone_var.varid == endpoint_cfg.var.varid:
                    config = (
                        config
                        + endpoint_var.special_name
                        + endpoint_var.phone_model.conf_seperator
                        + endpoint_cfg.var_val
                        + "\r\n"
                    )

            if endpoint_var.default_value is None:
                if endpoint_var.phone_var.default_value is not None:
                    config = (
                        config
                        + endpoint_var.special_name
                        + endpoint_var.phone_model.conf_seperator
                        + endpoint_var.phone_var.default_value
                        + "\r\n"
                    )
            else:
                config = (
                    config
                    + endpoint_var.special_name
                    + endpoint_var.phone_model.conf_seperator
                    + endpoint_var.default_value
                    + "\r\n"
                )

    return HttpResponse(config)


def get_vendor_media_essentials_lang(request, lang_code):
    return get_vendor_media_essentials(request, lang_code, "lang_")


def get_vendor_media_essentials_syslang(request, lang_code):
    return get_vendor_media_essentials(request, lang_code, "syslang_")


def get_vendor_media_essentials(request, lang_code, file_ident):
    try:
        media_file = CusMedia.objects.get(
            realm=1, name="vendor_mitel_sip_" + file_ident + lang_code + ".txt"
        )
    except CusMedia.DoesNotExist:
        return HttpResponse("Language not found!", status=404)

    return HttpResponse(media_file.data)


def get_vendor_media_essentials_cert(request, cert_name):
    try:
        media_file = CusMedia.objects.get(realm=1, name="crt_" + cert_name)
    except CusMedia.DoesNotExist:
        return HttpResponse("Cert not found!", status=404)

    return HttpResponse(media_file.data)
